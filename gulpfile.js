const gulp = require('gulp');
const fs = require('fs');

gulp.task('build', () => {
  const dir = './build';
  if (fs.existsSync(dir)) {
    deleteFolder(dir);
  }
  gulp.src('./src/*.ts')
    .pipe(gulp.dest('build/src'));
  gulp.src('./index.ts').pipe(gulp.dest('build'));
  gulp.src('./package.json').pipe(gulp.dest('build'));
});

gulp.task('removeGit', () => {
  deleteFolder('./.git');
});

function deleteFolder(path) {
  if (fs.existsSync(path)) {
    fs.readdirSync(path).forEach(function (file) {
      const curPath = path + "/" + file;
      if (fs.lstatSync(curPath).isDirectory()) { // recurse
        deleteFolder(curPath);
      } else { // delete file
        fs.unlinkSync(curPath);
      }
    });
    fs.rmdirSync(path);
  }
}
